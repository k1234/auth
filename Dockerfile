FROM openjdk:8
ADD auth.jar auth.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "auth.jar"]
