package com.kit.springsecurityjpa_jwt.service;

import java.util.Random;

public class RandomSalt {

    public String generateString()
    {
        Random ran = new Random();
        int top = 5;
        char data = ' ';
        String dat = "";

        for (int i=1; i<=top; i++) {
            data = (char)(ran.nextInt(25)+97);
            dat = data + dat;
        }

        return dat;
    }
}